from typing import List, Dict
from flask import Flask
import mysql.connector
import json
import jwt
import numpy as np
import pandas as pd
from flask import request
from flask import Response
import requests 


app = Flask(__name__)

@app.route('/')
def index() -> str:
    return json.dumps({'mensaje':  'Hola mundo'})


def func(x):
    return x + 1


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)


def test_answer():
    assert func(3) == 5

